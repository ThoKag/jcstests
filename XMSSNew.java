/*
 * Copyright 2012 Licel LLC.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//package com.licel.jcardsim.samples;

import javacard.framework.*;
import java.security.SecureRandom;
import java.text.ParseException;
import javacard.security.AESKey;
import javacard.framework.Util;
import javacard.security.KeyBuilder;
import javacard.security.MessageDigest;
import javacard.security.RandomData;
import javacard.security.KeyBuilder;
import java.util.Arrays;

/**
 * Basic HelloWorld JavaCard Applet.
 * @author LICEL LLC
 */
public class XMSSNew extends BaseApplet {

    /**
     * Instruction: say hello
     */
    private final static byte SAY_HELLO_INS = (byte) 0x01;
    /**
     * Instruction: say echo v2
     */
    private final static byte INIT_KEYS = (byte) 0x11;
    private final static byte SAY_ECHO2_INS = (byte) 0x03;
    /**
     * Instruction: get install params
     */
    private final static byte SAY_IPARAMS_INS = (byte) 0x04;    
    /**
     * Instruction: NOP
     */
    private final static byte NOP_INS = (byte) 0x02;
    /**
     * Instruction: queue data and return 61xx
     */
    private final static byte SAY_CONTINUE_INS = (byte) 0x06;
    /**
     * Instruction: CKYListObjects (http://pki.fedoraproject.org/images/7/7a/CoolKeyApplet.pdf 2.6.17)
     */
    private final static byte LIST_OBJECTS_INS = (byte) 0x58;
    /**
     * Instruction: "Hello Java Card world!" + Application Specific SW 9XYZ
     */
    private final static byte APPLICATION_SPECIFIC_SW_INS = (byte) 0x7;
    /**
     * Instruction: return maximum data.
     */
    private final static byte MAXIMUM_DATA_INS = (byte) 0x8;
    /**
     * Byte array representing "Hello Java Card world!" string.
     */
    private static byte[] helloMessage = new byte[]{
        0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, // "Hello "
        0x54, 0x68, 0x6f, 0x6d, 0x61, 0x73, 0x21 // "world !"
    };
    private byte[] echoBytes;
    private byte[] initParamsBytes;
    private final byte[] transientMemory;
    private static final short LENGTH_ECHO_BYTES = 256;
    private final byte[] tmp;
    private final byte[] nodes;
    private final RandomData randomData;


    public static final byte CLA = (byte)0x80;
    public static final byte INS_XMSS_KEYGEN = (byte)0x50;
    public static final byte INS_XMSS_INIT_KEYS = (byte)0x51;
    public static final byte INS_XMSS_GET_PK = (byte)0x52;
    public static final byte INS_XMSS_SIGN_INIT = (byte)0x53;
    public static final byte INS_XMSS_SIGN_WOTS = (byte)0x54;
    public static final byte INS_XMSS_SIGN_AUTHPATH = (byte)0x55;
    public static final byte INS_XMSS_SIGN_PREP = (byte)0x56;
    public static final byte INS_XMSS_INIT_KEYS_NO_COMPUTE = (byte)0x57;
    public static final byte INS_XMSS_INIT_NODES = (byte)0x58;
    public static final byte INS_XMSS_INIT_WOTS = (byte)0x59;
    /* For debugging purposes; there is no functional reason for this, but also
    no additional security risk. */
    public static final byte INS_XMSS_GET_NODES = (byte)0x5F;

    /**
     * Only this class's install method should create the applet object.
     * @param bArray the array containing installation parameters
     * @param bOffset the starting offset in bArray
     * @param bLength the length in bytes of the parameter data in bArray
     */
    protected XMSSNew(byte[] bArray, short bOffset, byte bLength, MessageDigest md) {
        System.out.println("XMSSNew - bOffset=" + bOffset + " bLength=" + bLength + " MessageDigest="+md.getAlgorithm());
        nodes = new byte[(short)(N * (2*D - 1) * (1 << (H / D)))];
        tmp = JCSystem.makeTransientByteArray((short) 512, JCSystem.CLEAR_ON_DESELECT);
        echoBytes = new byte[LENGTH_ECHO_BYTES];
        tmpShorts = JCSystem.makeTransientShortArray((short)67, JCSystem.CLEAR_ON_DESELECT);
        randomData = RandomData.getInstance(RandomData.ALG_SECURE_RANDOM);
        WOTSSignatures = new byte[(short)(WOTS_LEN * N * (D - 1))];
        signingRoot = JCSystem.makeTransientByteArray((short)32, JCSystem.CLEAR_ON_DESELECT);

        if (bLength > 0) {
            byte iLen = bArray[bOffset]; // aid length
            bOffset = (short) (bOffset + iLen + 1);
            byte cLen = bArray[bOffset]; // info length
            bOffset = (short) (bOffset + 3);
            byte aLen = bArray[bOffset]; // applet data length
            initParamsBytes = new byte[aLen];
            Util.arrayCopyNonAtomic(bArray, (short) (bOffset + 1), initParamsBytes, (short) 0, aLen);
        }
        transientMemory = JCSystem.makeTransientByteArray(LENGTH_ECHO_BYTES, JCSystem.CLEAR_ON_RESET);
        register();


        //XMSS portion
        this.md = md;
         publicKey = new byte[64];
         secretSeed = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);
         secretPRFKey = (AESKey)KeyBuilder.buildKey(KeyBuilder.TYPE_AES, KeyBuilder.LENGTH_AES_256, false);


    }    

    /**
     * This method is called once during applet instantiation process.
     * @param bArray the array containing installation parameters
     * @param bOffset the starting offset in bArray
     * @param bLength the length in bytes of the parameter data in bArray
     * @throws ISOException if the install method failed
     */

    public static void install(byte[] bArray, short bOffset, byte bLength)
            throws ISOException {
                System.out.println("install bArray=" + bArray + " bOffset=" + bOffset + " bLength=" + bLength);
        MessageDigest digestSHA256 = MessageDigest.getInstance(MessageDigest.ALG_SHA_256, false);
        System.out.println("generated md");
        new XMSSNew(bArray, bOffset,bLength,digestSHA256);
    }

    private static final char[] HEX_ARRAY = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = HEX_ARRAY[v >>> 4];
            hexChars[j * 2 + 1] = HEX_ARRAY[v & 0x0F];
        }
    return new String(hexChars);
    }

    public String byteToHex(byte num) {
    char[] hexDigits = new char[2];
        hexDigits[0] = Character.forDigit((num >> 4) & 0xF, 16);
        hexDigits[1] = Character.forDigit((num & 0xF), 16);
        return new String(hexDigits);
    }

    public String encodeHexString(byte[] byteArray) {
        StringBuffer hexStringBuffer = new StringBuffer();
        for (int i = 0; i < byteArray.length; i++) {
            hexStringBuffer.append(byteToHex(byteArray[i]));
        }
        return hexStringBuffer.toString();
    }
    /**
     * This method is called each time the applet receives APDU.
     */
    public void process(APDU apdu) {
        System.out.println("process(apdu:"+ Arrays.toString(apdu.getBuffer()));
        // good practice
        if(selectingApplet()) return;
        byte[] buffer = apdu.getBuffer();
        // Now determine the requested instruction:
        //System.out.println("received apdu " + Arrays.toString(buffer));
        switch (buffer[ISO7816.OFFSET_INS]) {
            case SAY_HELLO_INS:
                sayHello(apdu, (short)0x9000);
                return;
            case INIT_KEYS:
                System.out.println("test");
                initializeKeys(apdu, tmp, (short)0);
            case SAY_ECHO2_INS:
                sayEcho2(apdu);
                return;
            case SAY_IPARAMS_INS:
                sayIParams(apdu);
                return;
            case SAY_CONTINUE_INS:
                sayContinue(apdu);
                return;
            case LIST_OBJECTS_INS:
                listObjects(apdu);
                return;
            case APPLICATION_SPECIFIC_SW_INS:
                sayHello(apdu, (short)0x9B00);
                return;
            case MAXIMUM_DATA_INS:
                maximumData(apdu);
                return;
            case NOP_INS:
                return;
            case INS_XMSS_KEYGEN:
                generateKeypair(apdu, tmp, (short)0, randomData);
                break;
            case INS_XMSS_INIT_KEYS:
                initializeWithKeypair(apdu, tmp, (short)0);
                break;
            case INS_XMSS_INIT_KEYS_NO_COMPUTE:
                initializeKeys(apdu, tmp, (short)0);
                break;
            case INS_XMSS_INIT_WOTS:
                initializeWOTS(apdu, tmp, (short)0);
                break;
            case INS_XMSS_GET_NODES:
                getNodes(apdu);
                break;
            case INS_XMSS_GET_PK:
                getPublicKey(apdu);
                break;
            case INS_XMSS_SIGN_INIT:
                signInit(apdu, tmp, (short)0);
                break;
            case INS_XMSS_SIGN_WOTS:
                signWOTS(apdu, tmp, (short)0);
                break;
            case INS_XMSS_SIGN_AUTHPATH:
                signAuthPath(apdu, tmp, (short)0);
                break;
            case INS_XMSS_SIGN_PREP:
                signPrepNext(apdu, tmp, (short)0);
                break;
            default:
                // We do not support any other INS values
                ISOException.throwIt(ISO7816.SW_INS_NOT_SUPPORTED);
        }
    }

    /**
     * Sends hello message to host using given APDU.
     *
     * @param apdu APDU that requested hello message
     * @param sw response sw code
     */
    private void sayHello(APDU apdu, short sw) {
        System.out.println("sayHello");
        System.out.println("saying hello");
        // Here all bytes of the APDU are stored
        byte[] buffer = apdu.getBuffer();
        // receive all bytes
        // if P1 = 0x01 (echo)
        short incomeBytes = apdu.setIncomingAndReceive();
        byte[] echo = transientMemory;
        short echoLength;
        if (buffer[ISO7816.OFFSET_P1] == 0x01) {
            echoLength = incomeBytes;
            Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, echo, (short) 0, incomeBytes);
        } else {
            echoLength = (short) helloMessage.length;
            Util.arrayCopyNonAtomic(helloMessage, (short) 0, echo, (short) 0, (short) helloMessage.length);
        }
        // Tell JVM that we will send data
        apdu.setOutgoing();
        // Set the length of data to send
        apdu.setOutgoingLength(echoLength);
        // Send our message starting at 0 position
        apdu.sendBytesLong(echo, (short) 0, echoLength);
        // Set application specific sw
        if(sw!=0x9000) {
            ISOException.throwIt(sw);
        }
    }


    /**
     * echo v2
     */
    private void sayEcho2(APDU apdu) {
        System.out.println("sayEcho2");
        byte buffer[] = apdu.getBuffer();

        short bytesRead = apdu.setIncomingAndReceive();
        short echoOffset = (short) 0;

        while (bytesRead > 0) {
            Util.arrayCopyNonAtomic(buffer, ISO7816.OFFSET_CDATA, echoBytes, echoOffset, bytesRead);
            echoOffset += bytesRead;
            bytesRead = apdu.receiveBytes(ISO7816.OFFSET_CDATA);
        }

        apdu.setOutgoing();
        apdu.setOutgoingLength(echoOffset);
        // echo data
        apdu.sendBytesLong(echoBytes, (short) 0, echoOffset);

    }

    /**
     * echo install params
     */
    private void sayIParams(APDU apdu) {
        System.out.println("sayIParams");
        apdu.setOutgoing();
        apdu.setOutgoingLength((short)initParamsBytes.length);
        // echo install parmas
        apdu.sendBytesLong(initParamsBytes, (short) 0, (short)initParamsBytes.length);
    }   

    /**
     * send some hello data, and indicate there's more
     */
    private void sayContinue(APDU apdu) {
        System.out.println("sayContinue");
        byte[] echo = transientMemory;
        short echoLength = (short) 6;
        Util.arrayCopyNonAtomic(helloMessage, (short)0, echo, (short)0, (short)6);
        apdu.setOutgoing();
        apdu.setOutgoingLength(echoLength);
        apdu.sendBytesLong(echo, (short) 0, echoLength);
        ISOException.throwIt((short) (ISO7816.SW_BYTES_REMAINING_00 | 0x07));
    }


    /**
     * send the maximum amount of data the apdu will accept
     *
     * @param apdu APDU that requested hello message
     */
    private void maximumData(APDU apdu) {
        System.out.println("maximumData");
        short maxData = APDU.getOutBlockSize();
        byte[] buffer = apdu.getBuffer();
        Util.arrayFillNonAtomic(buffer, (short) 0, maxData, (byte) 0);
        apdu.setOutgoingAndSend((short) 0, maxData);
    }
    
    // prototype
    private void listObjects(APDU apdu)
    {
        System.out.println("listObjects");
        byte buffer[] = apdu.getBuffer();
        
	if (buffer[ISO7816.OFFSET_P2] != 0) {
            ISOException.throwIt((short)0x9C11);
        }
	
	byte expectedBytes = buffer[ISO7816.OFFSET_LC];
	
	if (expectedBytes < 14) {
            ISOException.throwIt(ISO7816.SW_WRONG_LENGTH);
        }
	
	ISOException.throwIt((short)0x9C12);
    }    




    private short signingState;
    private short nodeBytesReceived = 0;
    private short WOTSBytesReceived = 0;

    private final AESKey secretSeed;
    private final AESKey secretPRFKey;  /* PRFKEY */
    private final byte[] publicKey;  /* ROOT + PUBSEED */

    public static short nodesSent = 0;
    private final MessageDigest md;
    private final short[] tmpShorts;
    /* This array stores all WOTS signatures currently in use.
    Newly created signatures immediately overwrite older signatures */
    private final byte[] WOTSSignatures;

    /* Between APDUs, must store the current root node that we are signing.
    This also initially holds the message hash signed by the bottom WOTS. */
    private final byte[] signingRoot;

    /* This implementation assumes H/D <= 15, to simplify the arithmetic */
    public static final short D = 5;  /* Number of subtrees */
    /* This implementation assumes H <= 30; indices are two signed shorts */
    public static final short H = 20;  /* Height of the full tree */
    /* This implementation assumes N = 32 in various (unexpected?) ways */
    public static final short N = 32;  /* Size of a hash output */

    /* The index can be 2^20 for tree height 20, so we split into two shorts,
        i.e. the index is (indexHigh << 15 + indexLow)
        (it's awkward like this because short is a signed integer) */
    private short indexHigh;
    private short indexLow;
        /* During signing, we keep track of an index throughout the tree. */
    private short signingIdxHigh;
    private short signingIdxLow;

    public static final short ADDR_LAYER_LSBYTE = 3;
    public static final short ADDR_TREE_HIGH_LSBYTE = 7;
    public static final short ADDR_TREE_LOW_LSBYTE = 11;
    public static final short ADDR_TYPE_LSBYTE = 15;
    public static final short ADDR_OTS_LSBYTE = 19;
    public static final short ADDR_CHAIN_LSBYTE = 23;
    public static final short ADDR_HASH_LSBYTE = 27;
    public static final short ADDR_TREE_HEIGHT_LSBYTE = 23;
    public static final short ADDR_TREE_INDEX_LSBYTE = 27;
    public static final short ADDR_KEY_MASK_LSBYTE = 31;

    public static final byte DOMAINSEP_F = (byte)0;
    public static final byte DOMAINSEP_H = (byte)1;
    public static final byte DOMAINSEP_HASH = (byte)2;
    public static final byte DOMAINSEP_PRF = (byte)3;

    public static final byte ADDRTYPE_OTS = (byte)0;
    public static final byte ADDRTYPE_LTREE = (byte)1;
    public static final byte ADDRTYPE_HASHTREE = (byte)2;

    public static final short WOTS_W = 16;  /* Only supports WOTS_W == 16 */
    public static final short WOTS_LOG_W = 4;
    public static final short WOTS_LEN1 = 64;
    public static final short WOTS_LEN2 = 3;
    public static final short WOTS_LEN = 67;

//*****************************
    public void initializeKeys(APDU apdu, byte[] tmp, short o_tmp) {
        System.out.println("initializeKeys");
        /* Ensure that we're not actually ready to sign now, since nodes are
           not initialized properly (anymore). */
        signingState = -3;
        nodeBytesReceived = 0;
        WOTSBytesReceived = 0;
        nodesSent = 0;

        storeAPDU(apdu, tmp, o_tmp);

        secretSeed.setKey(tmp, o_tmp);
        secretPRFKey.setKey(tmp, (short)(o_tmp + 32));
        /* Zero out the secret key, just in case. */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)64, (byte)0);

        /* Note that the root is skipped and will be computed on the card */
        Util.arrayCopyNonAtomic(tmp, (short)(o_tmp + 64), publicKey, (short)32, (short)32);
    }

    public static short storeAPDU(APDU apdu, byte[] dest, short offset) {
        System.out.println("storeAPDU");
        short bytesRead = apdu.setIncomingAndReceive();
        /* Truncate to a byte to prevent sign extension. The jcardsim
           implementation returns 0xFF80 when really it means 0x80. */
        short numBytes = (short)(0xFF & apdu.getIncomingLength());

        byte[] buffer = apdu.getBuffer();
        short pos = 0;

        // Since numBytes may exceed the guaranteed CDATA buffer of 128 bytes
        while (pos < numBytes) {
            Util.arrayCopyNonAtomic(buffer, apdu.getOffsetCdata(), dest, (short)(pos + offset), bytesRead);
            pos += bytesRead;
            bytesRead = apdu.receiveBytes(apdu.getOffsetCdata());
        }
        return numBytes;
    }


    /* Generates a fresh keypair on the card, and initializes for signing */
    public void generateKeypair(APDU apdu, byte[] tmp, short o_tmp, RandomData rng) {
        System.out.println("generateKeypair - apdu:"+Arrays.toString(apdu.getBuffer()) + " tmp:" + Arrays.toString(tmp)+" o_tmp:" + o_tmp);
        rng.generateData(tmp, o_tmp, (short)64);  /* SKSEED + SKPRF */
        secretSeed.setKey(tmp, o_tmp);
        secretPRFKey.setKey(tmp, (short)(o_tmp + 32));
        /* Zero out the secret key, just in case. */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)64, (byte)0);

        rng.generateData(publicKey, (short)32, (short)32);  /* PUBSEED */

        /* Precompute leafs for all first trees */
        initializeNodes(tmp, o_tmp);

        /* Complete initialization */
        finalizeKeypair(tmp, o_tmp);
    }

        /* Initializes the nodes buffer to contain the leafs of all 0th trees.
       Expects tmp to have 32 bytes for the address and 13*N bytes for WOTS.
       This is a very costly operation. */
    public void initializeNodes(byte[] tmp, short o_tmp) {
        System.out.println("initializeNodes");
        short i, leafIdx;

        /* Compute the leafs of all initial trees on all layers */
        for (i = 0; i < D; i++) {
            for (leafIdx = 0; leafIdx < 1 << (H / D); leafIdx++) {
                /* Initialize the address */
                Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
                tmp[(short)(o_tmp + ADDR_LAYER_LSBYTE)] = (byte) i;
                /* The tree field can be left at zero, since this concerns the
                   first tree on each layer. */
                Util.setShort(tmp, (short)(o_tmp + ADDR_OTS_LSBYTE - 1), leafIdx);
                WOTSLeafGen(nodes, (short)(N * (i * (1 << (H / D)) + leafIdx)),
                            tmp, o_tmp, tmp, (short)(o_tmp + 32));
            }
        }
    }

        /* Finalizes a keypair after it has been seeded and supplied with
       precomputed leaf nodes. This is separated because several different
       approaches can be taken w.r.t. seeding and precomputing. */
    private void finalizeKeypair(byte[] tmp, short o_tmp) {
        System.out.println("finalizeKeypair");
        indexHigh = 0;
        indexLow = 0;

        /* The tree field can be left at zero, since it's the first tree */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
        tmp[(short)(o_tmp + ADDR_LAYER_LSBYTE)] = (byte)(D - 1);

        /* Use the leaf nodes of the topmost tree to compute the ROOT */
        /* Using the first 32 bytes of tmp for the address;
                 the next H/D * N bytes of tmp for auth path, ignoring result;
                 the next (H / D + 1) * N + 128 bytes as tmp */
        authAndRoot(tmp, (short)(o_tmp + 32), publicKey, (short)0,
                    nodes, (short)(N * ((D - 1) * (1 << (H / D)))),
                    (short)0,
                    tmp, o_tmp,
                    tmp, (short)(o_tmp + (H / D) * N + 32));

        signingState = 0;
    }

        /* root will contain the 32-byte output, addr is expected to be 32 bytes,
       tmp is needed for 13x32 intermediate bytes.
       This very strongly assumes that WOTS_W = 16. */
    private void WOTSLeafGen(byte[] root, short o_root,
                             byte[] addr, short o_addr,
                             byte[] tmp, short o_tmp) 
    {
        System.out.println("WOTSLeafGen");
        short i;
        short offset = 0;
        byte leaf = 0;

        md.reset();
        WOTSGetSeed(tmp, o_tmp, addr, o_addr, tmp, (short)(o_tmp + 64));

        /* tmp now contains [wots_seed] [??] [31x 0, DOMAINSEP_PRF] */

        /* Use treehash to compute the regular part of the ltree */
        for (leaf = 0; leaf < 64; leaf++) {
            WOTSComputeChainseed(tmp, (short)(o_tmp + 32),
                                 tmp, (short)(o_tmp + 64),
                                 tmp, o_tmp,
                                 leaf);

            /* tmp now contains [wots_seed] [chainseed] [31x 0, DOMAINSEP] [3x32 reserved] [6x32 reserved] */
            /* Reserved space is for hashh and the nodes on the treehash stack */
            addr[ADDR_TYPE_LSBYTE] = ADDRTYPE_OTS;
            addr[ADDR_CHAIN_LSBYTE] = leaf;

            /* Compute the next leaf of the ltree, push to the stack */
            WOTSChain(tmp, (short)(o_tmp + 6*32 + offset*32),
                      addr, o_addr,
                      tmp, (short)(o_tmp + 32),
                      tmp, (short)(o_tmp + 64), (short)(WOTS_W - 1));
            tmpShorts[offset] = 0;
            offset++;

            /* While the two topmost nodes on the stack are of the same height,
               hash them together. */
            while (offset >= 2 && tmpShorts[(short)(offset - 1)] ==
                                  tmpShorts[(short)(offset - 2)]) {
                addr[ADDR_TYPE_LSBYTE] = ADDRTYPE_LTREE;
                addr[ADDR_TREE_INDEX_LSBYTE] = (byte)(leaf >>> (tmpShorts[(short)(offset - 1)] + 1));
                addr[ADDR_TREE_HEIGHT_LSBYTE] = (byte)tmpShorts[(short)(offset - 1)];
                /* Overwrite one of the inputs, pop the other off the stack */
                hashh(tmp, (short)(o_tmp + 6*32 + (offset - 2)*32),
                      addr, o_addr,
                      tmp, (short)(o_tmp + 6*32 + (offset - 2)*32),
                      tmp, (short)(o_tmp + 6*32 + (offset - 1)*32),
                      tmp, (short)(o_tmp + 64));
                offset--;
                /* Note that the top-most node is now one layer higher. */
                tmpShorts[(short)(offset - 1)]++;
            }
        }

        /* The remaining three we do manually, to simplify ltree treehash;
           the alternative would be to make it generic w.r.t. pulling up nodes,
           but since we fix WOTS_W = 16, it is clear which nodes are lifted. */
        /* tmp[o_tmp + 6*32 + 0*32] contains (sub-)root of first 64 leafs. */

        /* Compute the three remaining leafs */
        addr[ADDR_TYPE_LSBYTE] = ADDRTYPE_OTS;
        for (leaf = 64; leaf < 67; leaf++) {
            WOTSComputeChainseed(tmp, (short)(o_tmp + 32),
                                 tmp, (short)(o_tmp + 64),
                                 tmp, o_tmp,
                                 leaf);
            addr[ADDR_CHAIN_LSBYTE] = leaf;
            WOTSChain(tmp, (short)(o_tmp + 6*32 + (leaf - 64 + 1)*32),
                      addr, o_addr,
                      tmp, (short)(o_tmp + 32),
                      tmp, (short)(o_tmp + 64), (short)(WOTS_W - 1));
        }

        addr[ADDR_TYPE_LSBYTE] = ADDRTYPE_LTREE;

        /* Combine the 65th and 66th leaf */
        addr[ADDR_TREE_INDEX_LSBYTE] = 64 >>> 1;
        addr[ADDR_TREE_HEIGHT_LSBYTE] = 0;
        hashh(tmp, (short)(o_tmp + 6*32 + 1*32),
              addr, o_addr,
              tmp, (short)(o_tmp + 6*32 + 1*32),
              tmp, (short)(o_tmp + 6*32 + 2*32),
              tmp, (short)(o_tmp + 64));

        /* Pull up the 67th leaf, combine */
        addr[ADDR_TREE_INDEX_LSBYTE] = 64 >>> 2;
        addr[ADDR_TREE_HEIGHT_LSBYTE] = 1;
        hashh(tmp, (short)(o_tmp + 6*32 + 1*32),
              addr, o_addr,
              tmp, (short)(o_tmp + 6*32 + 1*32),
              tmp, (short)(o_tmp + 6*32 + 3*32),
              tmp, (short)(o_tmp + 64));

        /* Pull up the result to the top of the tree, combine with sub-root */
        addr[ADDR_TREE_INDEX_LSBYTE] = 0;
        addr[ADDR_TREE_HEIGHT_LSBYTE] = 6;
        hashh(root, o_root,
              addr, o_addr,
              tmp, (short)(o_tmp + 6*32),
              tmp, (short)(o_tmp + 6*32 + 1*32),
              tmp, (short)(o_tmp + 64));
    }

    /* Computes auth path and root from leafs, authenticating leaf at leaf_idx,
       expecting an address that contains the layer and tree index set,
       and a tmp array that has space for (H / D + 1) * N + 128 bytes.
       outputs (H / D) * N bytes to auth, and N bytes to root. */
    public void authAndRoot(byte[] auth, short o_auth,
                            byte[] root, short o_root,
                            byte[] leafs, short o_leafs,
                            short leaf_idx,
                            byte[] addr, short o_addr,
                            byte[] tmp, short o_tmp) {
        System.out.println("authAndRoot");
        short offset = 0;
        short idx;
        short treeIdx;

        /* Reset everything except layer and tree */
        Util.arrayFillNonAtomic(addr, (short)(o_addr + ADDR_TYPE_LSBYTE + 1),
                                (short)16, (byte)0);
        addr[ADDR_TYPE_LSBYTE] = ADDRTYPE_HASHTREE;

        /* Ensure tmp contains leading zeroes; precondition for hashh */
        Util.arrayFillNonAtomic(tmp, (short)(o_tmp + (H/D + 1) * N),
                                (short)31, (byte)0);

        for (idx = 0; idx < (1 << (H/D)); idx++) {
            /* Add the next leaf node to the stack. */
            Util.arrayCopyNonAtomic(leafs, (short)(o_leafs + N*idx),
                                    tmp, (short)(o_tmp + offset*N), N);
            tmpShorts[offset] = 0;
            offset++;

            /* If this is a node we need for the auth path.. */
            if ((leaf_idx ^ 0x1) == idx) {
                Util.arrayCopyNonAtomic(tmp, (short)(o_tmp + (offset - 1)*N),
                                        auth, o_auth, N);
            }

            /* While the top-most nodes are of equal height.. */
            while (offset >= 2 && tmpShorts[(short)(offset - 1)] == tmpShorts[(short)(offset - 2)]) {
                /* Compute index of the new node, in the next layer. */
                treeIdx = (short)(idx >>> (tmpShorts[(short)(offset - 1)] + 1));

                /* Hash the top-most nodes from the stack together. */
                /* Note that tree height is the 'lower' layer, even though we use
                   the index of the new node on the 'higher' layer. This follows
                   from the fact that we address the hash function calls. */
                addr[ADDR_TREE_HEIGHT_LSBYTE] = (byte)tmpShorts[(short)(offset - 1)];
                addr[ADDR_TREE_INDEX_LSBYTE] = (byte)treeIdx;

                /* Overwrite one of the inputs, pop the other off the stack */
                hashh(tmp, (short)(o_tmp + (offset - 2)*N),
                      addr, o_addr,
                      tmp, (short)(o_tmp + (offset - 2)*N),
                      tmp, (short)(o_tmp + (offset - 1)*N),
                      /* There are at most H/D + 1 nodes on the stack */
                      tmp, (short)(o_tmp + (H/D + 1) * N));
                offset--;
                /* Note that the top-most node is now one layer higher. */
                tmpShorts[(short)(offset - 1)]++;

                /* If this is a node we need for the auth path.. */
                if ((short)((leaf_idx >>> tmpShorts[(short)(offset - 1)]) ^ 0x1) == treeIdx) {
                    Util.arrayCopyNonAtomic(tmp, (short)(o_tmp + (offset - 1)*N),
                                            auth, (short)(o_auth + tmpShorts[(short)(offset - 1)]*N), N);
                }
            }
        }
        Util.arrayCopyNonAtomic(tmp, o_tmp, root, o_root, N);
    }

        /* Computes an OTS seed; zeroes out 32 bytes in tmp as a side effect */
    private void WOTSGetSeed(byte[] seed, short o_seed,
                             byte[] addr, short o_addr,
                             byte[] tmp, short o_tmp) {
        System.out.println("WOTSGetSeed");
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)31, (byte)0);
        tmp[(short)(o_tmp + 31)] = DOMAINSEP_PRF;
        md.update(tmp, o_tmp, N);
        secretSeed.getKey(tmp, o_tmp);
        md.update(tmp, o_tmp, (short)32);

        /* Zero out the secret seed, just in case. */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);

        /* Zero out CHAIN, HASH and KEY_AND_MASK fields */
        Util.arrayFillNonAtomic(addr, (short)(o_addr + ADDR_CHAIN_LSBYTE-3),
                                (short)12, (byte)0);

        md.doFinal(addr, o_addr, (short)32, seed, o_seed);
    }

        /* Writes the seed to `out`, expects domainSep to be [31x ZERO, 1x ??] */
    private void WOTSComputeChainseed(byte[] out, short o_out,
                                      byte[] domainSep, short o_domainSep,
                                      byte[] WOTSSeed, short o_WOTSSeed,
                                      byte chain) {
        System.out.println("WOTSComputeChainseed");
        domainSep[(short)(o_domainSep + 31)] = DOMAINSEP_PRF;
        md.update(domainSep, o_domainSep, (short)32);
        md.update(WOTSSeed, o_WOTSSeed, (short)32);
        domainSep[(short)(o_domainSep + 31)] = chain;
        md.doFinal(domainSep, o_domainSep, (short)32, out, o_out);
    }

        /* Iterates F calls to compute the WOTS chaining output.
       Destroys the input, allows overlapping input and output.
       Requirements on tmp inherit from hashf. */
    public void WOTSChain(byte[] out, short o_out,
                          byte[] addr, short o_addr,
                          byte[] in, short o_in,
                          byte[] tmp, short o_tmp,
                          short chainlen) {
        System.out.println("WOTSChain");
        for (short i = 0; i < chainlen; i++) {
            addr[(short)(o_addr + ADDR_HASH_LSBYTE)] = (byte)(0xFF & i);
            hashf(in, o_in, addr, o_addr, in, o_in, tmp, o_tmp);
        }
        Util.arrayCopyNonAtomic(in, o_in, out, o_out, N);
    }

    /* Assumes tmp starts with 31 zeroes and has 128 bytes available in total.
       Preserves the input, which is useful in stored hash trees.*/
    private void hashh(byte[] out, short o_out,
                       byte[] addr, short o_addr,
                       byte[] in1, short o_in1,
                       byte[] in2, short o_in2,
                       byte[] tmp, short o_tmp) {
        //System.out.println("hashh");
        short i;

        tmp[(short)(o_tmp + N-1)] = DOMAINSEP_PRF;

        /* Compute the mask */
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(publicKey, N, N);  /* PUBSEED */
        addr[(short)(o_addr + ADDR_KEY_MASK_LSBYTE)] = 1;
        md.doFinal(addr, o_addr, (short)32, tmp, (short)(o_tmp + 2*N));

        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(publicKey, N, N);  /* PUBSEED */
        addr[(short)(o_addr + ADDR_KEY_MASK_LSBYTE)] = 2;
        md.doFinal(addr, o_addr, (short)32, tmp, (short)(o_tmp + 3*N));

        /* Mask the input; preserve the input by masking into tmp */
        for (i = 0; i < 32; i++) {
            tmp[(short)(o_tmp + 2*N + i)] ^= in1[(short)(o_in1 + i)];
        }
        for (i = 0; i < 32; i++) {
            tmp[(short)(o_tmp + 3*N + i)] ^= in2[(short)(o_in2 + i)];
        }

        /* Compute the key */
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(publicKey, N, N);  /* PUBSEED */
        addr[(short)(o_addr + ADDR_KEY_MASK_LSBYTE)] = 0;
        md.doFinal(addr, o_addr, (short)32, tmp, (short)(o_tmp + N));

        /* Combine to compute the output */
        tmp[(short)(o_tmp + N-1)] = DOMAINSEP_H;
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(tmp, (short)(o_tmp + N), N);  /* Key */
        md.doFinal(tmp, (short)(o_tmp + 2*N), (short)(2*N),  /* Masked input */
                   out, o_out);
    }

        /* Assumes that tmp starts with 31 zeroes, has a total of 64 bytes.
       Destroys the input buffer's contents; input and output can overlap. */
    private void hashf(byte[] out, short o_out,
                       byte[] ots_addr, short o_addr,
                       byte[] in, short o_in,
                       byte[] tmp, short o_tmp) {
       // System.out.print("hashf");
        short i;

        tmp[(short)(o_tmp + N-1)] = DOMAINSEP_PRF;

        /* Compute the mask */
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(publicKey, N, N);  /* PUBSEED */
        ots_addr[(short)(o_addr + ADDR_KEY_MASK_LSBYTE)] = 1;
        md.doFinal(ots_addr, o_addr, (short)32, tmp, (short)(o_tmp + N));

        /* Mask the input */
        for (i = 0; i < N; i++) {
            in[(short)(o_in + i)] ^= tmp[(short)(o_tmp + N + i)];
        }

        /* Compute the key */
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(publicKey, N, N);  /* PUBSEED */
        ots_addr[(short)(o_addr + ADDR_KEY_MASK_LSBYTE)] = 0;
        md.doFinal(ots_addr, o_addr, (short)32, tmp, (short)(o_tmp + N));

        /* Combine to compute the output */
        tmp[(short)(o_tmp + N-1)] = DOMAINSEP_F;
        md.update(tmp, o_tmp, N);  /* Domain separator */
        md.update(tmp, (short)(o_tmp + N), N);  /* Key */
        md.doFinal(in, o_in, N, out, o_out);  /* Masked input */
    }
 /* Initializes the card using randomness generated outside the card. This
       may be useful for situations where there are additional requirements
       on the security of the RNG. Expects 3 seeds: SKSEED, SKPRF, PUBSEED */
    public void initializeWithKeypair(APDU apdu, byte[] tmp, short o_tmp) {
        System.out.println("initializeWithKeypair");
        initializeKeys(apdu, tmp, o_tmp);

        /* Precompute leafs for all first trees */
        initializeNodes(tmp, o_tmp);

        /* Complete initialization */
        finalizeKeypair(tmp, o_tmp);
    }

    public void initializeWOTS(APDU apdu, byte[] tmp, short o_tmp) {
        System.out.println("initializeWOTS");
        /* Can only upload WOTS signatures if we've set up nodes */
        if (signingState != -2) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }
        short bytesReceived = storeAPDU(apdu, tmp, o_tmp);
        short bytesNeeded = (short)((WOTS_LEN * N * (D - 1)) - WOTSBytesReceived);

        if (bytesReceived < bytesNeeded) {
            Util.arrayCopyNonAtomic(tmp, o_tmp, WOTSSignatures, WOTSBytesReceived, bytesReceived);
            WOTSBytesReceived += bytesReceived;
        }
        else {
            Util.arrayCopyNonAtomic(tmp, o_tmp, WOTSSignatures, WOTSBytesReceived, bytesNeeded);
            WOTSBytesReceived += bytesNeeded;
            /* Complete initialization */
            finalizeKeypair(tmp, o_tmp);
        }
    }

    /* For debugging purposes, get the nodes */
    public void getNodes(APDU apdu) {
        System.out.println("getNodes");
        nodesSent++;
        apdu.setOutgoing();
        apdu.setOutgoingLength((short)32);
        apdu.sendBytesLong(nodes, (short)(32 * (nodesSent - 1)), (short)32);
    }

        /* Output ROOT and PUBSEED */
    public void getPublicKey(APDU apdu) {
        System.out.println("getPublicKey");
        apdu.setOutgoing();
        apdu.setOutgoingLength((short)64);
        apdu.sendBytesLong(publicKey, (short)0, (short)64);
    }

        /* Initial method to start creating a signature. Expects the signing state
       to be in the zero-state.
       Reads the message from the APDU, hashes it and returns the first parts
       of the signature: indexBytes and R. */
    public void signInit(APDU apdu, byte[] tmp, short o_tmp) {
        System.out.println("signInit");
        if (signingState != 0) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }

        short messageLength = storeAPDU(apdu, tmp, (short)(o_tmp + 32));
        byte[] buffer = apdu.getBuffer();

        /* Initialize the index used for the current signature */
        signingIdxHigh = indexHigh;
        signingIdxLow = indexLow;

        /* Increment the index to be used for the next signature; this must be
           done before actually signing, to ensure one signature per leaf. */
        if (indexLow == 32767) {
            /* Carry into indexHigh when it would otherwise overflow 15 bits */
            indexHigh++;
            /* Worst case, if we break off here, we skip 2^15 signatures rather
               than redoing 2^15 with the same indexHigh */
            indexLow = 0;
        }
        else {
            indexLow++;
        }

        short indexBytes = (H + 7) / 8;
        /* Convert the shorts to consecutive bytes */
        indexToBytes(buffer, (short)0, signingIdxHigh, signingIdxLow);

        /* Compute R = PRF(index_as_32_bytes, key) */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)31, (byte)0);
        tmp[(short)(o_tmp + 31)] = DOMAINSEP_PRF;
        md.reset();
        md.update(tmp, o_tmp, (short)32);
        secretPRFKey.getKey(tmp, o_tmp);
        md.update(tmp, o_tmp, (short)32);
        /* Zero out the PRF key */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
        /* Since tmp is at all-zero, this sets it to [28x zero, idx] */
        indexToBytes(tmp, (short)(o_tmp + 28), signingIdxHigh, signingIdxLow);
        md.doFinal(tmp, o_tmp, (short)32, buffer, (short)4);

        /* Compute the message hash = H(R, ROOT, index, message) */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)31, (byte)0);
        tmp[(short)(o_tmp + 31)] = DOMAINSEP_HASH;
        md.update(tmp, o_tmp, (short)32);  /* Domain separator */
        md.update(buffer, (short)4, N);  /* R */
        md.update(publicKey, (short)0, N);  /* ROOT */
        /* Since the first 31 bytes are zero, this sets tmp to [28x zero, idx] */
        indexToBytes(tmp, (short)(o_tmp + 28), signingIdxHigh, signingIdxLow);
        md.update(tmp, o_tmp, (short)32);  /* Idx */
        md.doFinal(tmp, (short)(o_tmp + 32), messageLength, signingRoot, (short)0);  /* Message */

        apdu.setOutgoing();
        signingState++;
        /* indexBytes bytes for the index, 32 bytes for R */
        apdu.setOutgoingLength((short)(N + indexBytes));
        /* Skip some bytes if indexBytes should be less than the max. 4 */
        apdu.sendBytes((short)(4 - indexBytes), (short)(N + indexBytes));
    }

    public void signWOTS(APDU apdu, byte[] tmp, short o_tmp) {
        System.out.println("signWOTS");
        if (signingState < 1 || ((signingState - 1) & 15) > 8) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }

        /* Figure out what the current leaf node was */
        short currLow = (short)(indexLow - 1);
        short currHigh = indexHigh;
        short currLayer = (short)((short)(signingState - 1) >>> 4);

        /* If it underflowed, carry from currHigh. */
        if (currLow == -1) {
            currLow = 32767;
            currHigh -= 1;
        }

        /* Send 8x32 bytes for normal chains, 3x32 for the checksum */
        /* jcardsim breaks for some reason when Le = 0x00 implying 256 bytes,
           so change this to 255 when running in jcardsim for debugging. */
        short outlen = (short)((((signingState - 1) & 15) == 8) ? 96 : 256);
        apdu.setOutgoing();
        apdu.setOutgoingLength(outlen);

        /* It could be that we have previously cached this signature */
        /* The first signature is exceptional; there is no previous index,
           so we must be careful wrt the result of `firstSameLayer`. */
        if (!(indexLow == 1 && indexHigh == 0 && currLayer == 0)) {
            if ((indexLow == 1 && indexHigh == 0 && currLayer > 0) ||
                currLayer >= firstSameLayer(currHigh, currLow)) {
                signingState++;
                apdu.sendBytesLong(
                    WOTSSignatures,
                    (short)(((signingState - 2) & 15) * 256 + (currLayer-1) * WOTS_LEN * N),
                    outlen);
                return;
            }
        }

        /* Use tmp as [address] [ots seed] [chainseed] [31x 0, DOMAINSEP] */

        Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
        /* Set subtree address */
        tmp[(short)(o_tmp + ADDR_LAYER_LSBYTE)] = (byte)currLayer;
        setTreeAddrFromLeaf(tmp, o_tmp, signingIdxHigh, signingIdxLow);

        tmp[(short)(o_tmp + ADDR_TYPE_LSBYTE)] = ADDRTYPE_OTS;

        /* Since we assume that H/D <= 15, we can freely use only the low short
           for the OTS leaf index */
        /* The -3 is needed because indexToBytes writes 4 bytes. */
        /* Mask out any bits that exceed the current tree */
        indexToBytes(tmp, (short)(ADDR_OTS_LSBYTE - 3), (short)0,
                     (short)(signingIdxLow & ((1 << (H/D)) - 1)));

        WOTSGetSeed(tmp, (short)(o_tmp + 32),
                    tmp, o_tmp,
                    tmp, (short)(o_tmp + 96));

        /* tmp now contains [address] [ots seed] [??] [31x 0, DOMAINSEP_PRF] */

        /* Begin signing the value in signingRoot using WOTS */

        /* First compute the lengths of the chains we need.
           TODO: in principle each signing stage only needs a portion of these,
           but splitting the computation is a hassle. No real speed gain. */
        chainLengths(tmpShorts, signingRoot, tmp, (short)(o_tmp + 64));

        /* Compute which chain is the starting chain */
        short chain = (short)(8 * ((signingState - 1) & 15));

        /* In principle 8 chains = 256 bytes per APDU */
        for (short i = 0; i < 8; i++) {
            /* Since the checksum is not a multiple of 8, the last block of
               chains must terminate early */
            if ((short)(chain + i) >= WOTS_LEN) {
                break;
            }

            WOTSComputeChainseed(tmp, (short)(o_tmp + 64),
                                 tmp, (short)(o_tmp + 96),
                                 tmp, (short)(o_tmp + 32),
                                 (byte)(chain + i));

            tmp[(short)(o_tmp + ADDR_CHAIN_LSBYTE)] = (byte)(chain + i);
            /* tmp now contains: [address] [ots seed] [chainseed]
                                 [31x 0, DOMAINSEP_PRF]
                                 [reservation for f in WOTSChain]
                                 [.. chain output..] */
            if (currLayer == 0) {
                WOTSChain(tmp, (short)(o_tmp + 160 + i*32),
                          tmp, o_tmp,
                          tmp, (short)(o_tmp + 64),
                          tmp, (short)(o_tmp + 96),
                          tmpShorts[(short)(chain + i)]);
            }
            else {
                WOTSChain(WOTSSignatures, (short)(((signingState - 1) & 15) * 256 + i*32 + (currLayer-1) * WOTS_LEN * N),
                          tmp, o_tmp,
                          tmp, (short)(o_tmp + 64),
                          tmp, (short)(o_tmp + 96),
                          tmpShorts[(short)(chain + i)]);
            }
        }
        signingState++;
        if (currLayer == 0) {
            apdu.sendBytesLong(
                tmp,
                (short)(o_tmp + 160),
                outlen);
        }
        else{
            apdu.sendBytesLong(
                WOTSSignatures,
                (short)(((signingState - 2) & 15) * 256 + (currLayer-1) * WOTS_LEN * N),
                outlen);
        }
    }

        /* Expects signingState-1 to be 9 mod 16. Computes the authentication path
       that is necessary to authenticate the WOTS public key. */
    public void signAuthPath(APDU apdu, byte[] tmp, short o_tmp) {
        if (((signingState - 1) & 15) != 9) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }

        /* Since we assume that H/D <= 15, we can freely use only the low short
           for the OTS leaf index */
        /* Mask out any bits that exceed the current tree */
        short leafIdx = (short)(signingIdxLow & ((1 << (H/D)) - 1));
        /* Compute whether this is an even or odd tree, which indicates the
           location of precomputed leaf nodes in the `nodes` array */
        short treeParity = (short)(deriveTreeIdxLow(signingIdxHigh, signingIdxLow) & 0x1);

        /* Initialize the address */
        Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
        /* Set subtree address */
        tmp[(short)(o_tmp + ADDR_LAYER_LSBYTE)] = (byte)((short)(signingState - 1) >>> 4);
        setTreeAddrFromLeaf(tmp, o_tmp, signingIdxHigh, signingIdxLow);

        /* Store the authentication path in next H/D * N bytes, in tmp;
           use the remaining (H / D + 1) * N + 128 bytes as temporary space
           for the authAndRoot function */
        authAndRoot(tmp, (short)(o_tmp + 32), signingRoot, (short)0,
                    /* The relevant leaf nodes have been precomputed */
                    nodes, (short)(N * (D*treeParity + tmp[ADDR_LAYER_LSBYTE]) * (1 << (H / D))),
                    leafIdx,
                    tmp, o_tmp,
                    tmp, (short)(o_tmp + (H / D) * N + 32));

        /* Move the signing index one layer up */
        signingIdxLow = deriveTreeIdxLow(signingIdxHigh, signingIdxLow);
        signingIdxHigh = deriveTreeIdxHigh(signingIdxHigh);

        /* If this was the topmost layer.. */
        if ((short)((short)(signingState - 1) >>> 4) == D - 1) {
            signingState++;  /* Continue to preparation step */
        }
        else {
            signingState += (short)7;  /* Skip over assigned states */
        }

        /* Output the authentication path */
        apdu.setOutgoing();
        apdu.setOutgoingLength((short)(N * H / D));
        apdu.sendBytesLong(tmp, (short)(o_tmp + 32), (short)(N * H / D));
    }

        /* Computes a leaf node in the 'next' tree on every layer where a leaf
       node was just consumed by the signature we created. This implies the
       bottom layer always gets one leaf computation, the next layer once every
       two signatures, etc.
       This function is not necessary for completeness of the signature, but
       it is enforced before the next signature is created. That is not
       strictly necessary, as there is roughly H/D signatures leeway, but
       simplifies the implementation. */
    public void signPrepNext(APDU apdu, byte[] tmp, short o_tmp) {
        if (((signingState - 1) & 15) != 10) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }
        short i;

        /* Compute the index of the current signature (note that indexLow has
           already been incremented, and signingLow has been destroyed). */
        short currLow = (short)(indexLow - 1);
        short currHigh = indexHigh;
        short nextLow, nextHigh;
        short nextTreeParity;

        /* If it underflowed, carry from currHigh. */
        if (currLow == -1) {
            currLow = 32767;
            currHigh -= 1;
        }

        short unchangedLayer = firstSameLayer(currHigh, currLow);

        /* Prepare on every layer, except for the topmost single tree */
        for (i = 0; i < D - 1; i++) {
            /* If this is the very first index, we disregard prev entirely;
               the next trees are empty on all layers, so we want to compute
               a leaf node for each. */
            if (!(indexLow == 1 && indexHigh == 0)) {
                /* If the current and previous index have converged, none of
                   the indices on the next layers will have changed, so we do
                   not need to compute new leafs. */
                if (i == unchangedLayer) {
                    break;
                }
            }

            /* If 'currLow + tree width' fits within 15 bits.. (comparing in
               this way is necessary to avoid an overflow) */
            if ((short)(32767 - currLow) >= 1 << (H/D)) {
                nextLow = (short)(currLow + (1 << (H/D)));
                nextHigh = currHigh;
            }
            else {
                /* Otherwise loop around mod 2^15. Again avoid overflow into
                   signed integers. */
                nextLow = (short)((1 << (H/D)) - (32767 - currLow));
                nextHigh = (short)(currHigh + 1);
            }

            /* Initialize the address */
            Util.arrayFillNonAtomic(tmp, o_tmp, (short)32, (byte)0);
            tmp[(short)(o_tmp + ADDR_LAYER_LSBYTE)] = (byte) i;
            setTreeAddrFromLeaf(tmp, o_tmp, nextHigh, nextLow);

            /* Since we assume that H/D <= 15, we can freely use only the low
               short for the OTS leaf index */
            /* Mask out any bits that exceed the current tree */
            short leafIdx = (short)(nextLow & ((1 << (H/D)) - 1));
            Util.setShort(tmp, (short)(o_tmp + ADDR_OTS_LSBYTE - 1), leafIdx);

            /* Move the indices one layer up */
            currLow = deriveTreeIdxLow(currHigh, currLow);
            currHigh = deriveTreeIdxHigh(currHigh);

            /* Parity of next tree is inverse of current tree parity */
            nextTreeParity = (short)(1 - (currLow & 0x1));

            WOTSLeafGen(nodes, (short)(N * ((nextTreeParity*D + i) * (1 << (H / D)) + leafIdx)),
                               tmp, o_tmp, tmp, (short)(o_tmp + 32));
        }

        signingState = 0;
        /* The index has already been incremented in signInit; waiting until
           here would have been a security risk. */
    }

        /* Converts an index consisting of two 15-bit positive shorts to 4 bytes.
       This is required for the signature, but also for addresses. */
    private void indexToBytes(byte[] out, short o_out, short high, short low) {
        Util.setShort(out, o_out, high);
        Util.setShort(out, (short)(o_out + 2), low);
        /* Carry a bit from the 2nd to the 3rd byte */
        out[(short)(o_out + 2)] |= 0xFF & ((out[(short)(o_out + 1)] & 1) << 7);
        out[(short)(o_out + 1)] >>>= 1;
        /* Carry a bit from the 1st to the 2nd byte */
        out[(short)(o_out + 1)] |= 0xFF & ((out[o_out] & 1) << 7);
        out[o_out] >>>= 1;
        /* Note that we only need to carry one bit; the other irregular bit is
           the most significant bit of the 1st byte, and will remain zero. */
    }

    /* Returns the first layer on which the index and its predecessor converge.
       Yields undefined results for input (0, 0), i.e. the first index. */
    public short firstSameLayer(short idxHigh, short idxLow) {
        /* Roll back one index, to see which leafs have changed */
        short prevLow = (short)(idxLow - 1);
        short prevHigh = idxHigh;
        short i;

        /* If it underflowed, carry from prevHigh. prevHigh also underflows
           for the first signature; one should ignore this case. */
        if (prevLow == -1) {
            prevLow = 32767;
            prevHigh -= 1;
        }

        for (i = 1; i < D; i++) {
            idxLow = deriveTreeIdxLow(idxHigh, idxLow);
            idxHigh = deriveTreeIdxHigh(idxHigh);
            prevLow = deriveTreeIdxLow(prevHigh, prevLow);
            prevHigh = deriveTreeIdxHigh(prevHigh);

            if (idxLow == prevLow && idxHigh == prevHigh) {
                return i;
            }
        }
        return D;
    }

        /* Sets the tree field based on a leaf index. */
    public void setTreeAddrFromLeaf(byte[] addr, short o_addr, short idxHigh, short idxLow) {
        indexToBytes(addr, (short)(o_addr + ADDR_TREE_LOW_LSBYTE - 3),
                     deriveTreeIdxHigh(idxHigh),
                     deriveTreeIdxLow(idxHigh, idxLow));
        /* Can safely ignore ADDR_TREE_HIGH_LSBYTE since we assume H <= 30 */
    }

      /* Takes a message and derives the corresponding chain lengths.
       Required space in tmp is inherited from WOTSChecksum. */
    private void chainLengths(short[] lengths, byte[] msg,
                              byte[] tmp, short o_tmp) {
        baseW(lengths, (short)0, msg, (short)0, WOTS_LEN1);
        WOTSChecksum(lengths, lengths, tmp, o_tmp);
    }

        /* Computes the low 15 bits of a tree index based on a leaf index;
       effectively performs a division by 2^(H/D) */
    public short deriveTreeIdxLow(short idxHigh, short idxLow) {
        return (short)((idxLow >>> (H/D) + (32767 & (idxHigh << (15 - H/D)))));
    }

        /* Computes the high 15 bits of a tree index based on a leaf index;
       since its result is strictly smaller, only requires high part of idx;
       effectively performs a division by 2^(H/D) */
    public short deriveTreeIdxHigh(short idxHigh) {
        return (short)(idxHigh >>> (H/D));
    }

        /* Converts msg to baseW and writes results to lengths array, for the
       purpose of creating WOTS chains. */
    private void baseW(short[] lengths, short offset,
                       byte[] msg, short o_msg, short msgLength) {
        short in = 0;
        short out = 0;
        byte total = 0;
        short bits = 0;
        short consumed;

        for (consumed = 0; consumed < msgLength; consumed++) {
            if (bits == 0) {
                total = msg[(short)(o_msg + in)];
                in++;
                bits += 8;
            }
            bits -= WOTS_LOG_W;
            lengths[(short)(out + offset)] = (short)((total >>> bits) & (WOTS_W - 1));
            out++;
        }
    }
    
    public void initializeWithNodes(APDU apdu, byte[] tmp, short o_tmp) {
        /* Can only upload nodes if we're setting up a new key */
        if (signingState != -3) {
            ISOException.throwIt(ISO7816.SW_CONDITIONS_NOT_SATISFIED);
        }
        short bytesReceived = storeAPDU(apdu, tmp, o_tmp);
        short bytesNeeded = (short)(N * D * (1 << (H / D)) - nodeBytesReceived);

        if (bytesReceived < bytesNeeded) {
            Util.arrayCopyNonAtomic(tmp, o_tmp, nodes, nodeBytesReceived, bytesReceived);
            nodeBytesReceived += bytesReceived;
        }
        else {
            Util.arrayCopyNonAtomic(tmp, o_tmp, nodes, nodeBytesReceived, bytesNeeded);
            nodeBytesReceived += bytesNeeded;
            /* Continue to uploading WOTS signatures */
            signingState = -2;
        }
    }
        /* Computes the WOTS+ checksum over a message (in base WOTS_W).
       Requires 2 bytes of temporary data in tmp. */
    private void WOTSChecksum(short[] checksumLengths, short[] msgLengths,
                              byte[] tmp, short o_tmp) {
        short csum = 0; /* At most WOTS_LEN1 * WOTS_W */
        short i;

        /* Compute checksum */
        for (i = 0; i < WOTS_LEN1; i++) {
            csum += (short)(WOTS_W - 1 - msgLengths[i]);
        }

        /* Ensure expected empty (zero) bits are the least significant bits. */
        csum <<= (short)((8 - ((short)(WOTS_LEN2 * WOTS_LOG_W) % 8)));
        /* For W = 16 N = 32, the checksum fits in 10 < 15 bits */
        Util.setShort(tmp, o_tmp, csum);

        /* Convert checksum to base W */
        baseW(checksumLengths, WOTS_LEN1, tmp, o_tmp, WOTS_LEN2);
    }

}


