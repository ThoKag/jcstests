import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.digests.SHA512Digest;
import org.bouncycastle.pqc.crypto.xmss.XMSS;
import org.bouncycastle.pqc.crypto.xmss.XMSSMT;
import org.bouncycastle.pqc.crypto.xmss.XMSSNode;
import org.bouncycastle.pqc.crypto.xmss.XMSSParameters;
import org.bouncycastle.pqc.crypto.xmss.XMSSSignature;
import org.bouncycastle.pqc.crypto.xmss.XMSSUtil;
import org.bouncycastle.pqc.crypto.xmss.*;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.util.Arrays;
import org.bouncycastle.util.encoders.Hex;
import java.security.SecureRandom;

import com.licel.jcardsim.utils.AutoResetEvent;
import com.licel.jcardsim.base.SimulatorSystem;
import com.licel.jcardsim.smartcardio.CardSimulator;
import com.licel.jcardsim.smartcardio.CardTerminalSimulator;
import com.licel.jcardsim.utils.ByteUtil;
import com.licel.jcardsim.utils.AIDUtil;
import java.lang.reflect.Field;
import javacard.framework.ISO7816;
import javacard.framework.AID;
import javacard.framework.JCSystem;
import javacard.framework.Shareable;
import javacard.framework.SystemException;
import javacard.framework.TransactionException;
import javacard.security.MessageDigest;

import javax.smartcardio.*;
import java.security.NoSuchAlgorithmException;
import java.security.Security;
//import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;



public class CardTest
{
static int xmss_params_func;
static int xmss_params_n;
static int xmss_params_wots_w;
static int xmss_params_wots_log_w;
static int xmss_params_wots_len1;
static int xmss_params_wots_len2;
static int xmss_params_wots_len;
static int xmss_params_wots_sig_bytes;
static int xmss_params_full_height;
static int xmss_params_tree_height;
static int xmss_params_d;
static int xmss_params_index_bytes;
static int xmss_params_sig_bytes;
static int xmss_params_pk_bytes;
static long xmss_params_sk_bytes;
static int xmss_params_bds_k;
static final int XMSS_MLEN = 32;
 
       public static void main (String[] args)
       {
 
       	/*	// 1. Create simulator

        CardSimulator simulator = new CardSimulator();

        byte[] appletAIDBytes = new byte[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
        AID appletAID = new AID(appletAIDBytes, (short) 0, (byte) appletAIDBytes.length);

        simulator.installApplet(appletAID, XMSSNew.class);
        simulator.selectApplet(appletAID);

        // test NOP
        byte[] response = simulator.transmitCommand(new byte[]{0x00, 0x02, 0x00, 0x00});
        ByteUtil.requireSW(response, 0x9000);
    
		*/

xmss_params_func = 0;
    xmss_params_d = 5;
    xmss_params_n = 32;
    xmss_params_full_height = 20;

    xmss_params_tree_height = xmss_params_full_height / xmss_params_d;
    xmss_params_wots_w = 16;
    xmss_params_wots_log_w = 4;
    xmss_params_wots_len1 = 8 * xmss_params_n / xmss_params_wots_log_w;
    /* len_2 = floor(log(len_1 * (w - 1)) / log(w)) + 1 */
    xmss_params_wots_len2 = 3;
    xmss_params_wots_len = xmss_params_wots_len1 + xmss_params_wots_len2;
    xmss_params_wots_sig_bytes = xmss_params_wots_len * xmss_params_n;
    /* Round index_bytes up to nearest byte. */
    xmss_params_index_bytes = (xmss_params_full_height + 7) / 8;
    xmss_params_sig_bytes = (xmss_params_index_bytes + xmss_params_n
                         + xmss_params_d * xmss_params_wots_sig_bytes
                         + xmss_params_full_height * xmss_params_n);

    xmss_params_pk_bytes = 2 * xmss_params_n;
    xmss_params_sk_bytes = xmss_params_index_bytes+4*xmss_params_n;


	byte[] pk = new byte [xmss_params_pk_bytes];
    byte[] pkout = new byte [xmss_params_pk_bytes];
    byte[] sk = new byte [(int)xmss_params_sk_bytes];
    //byte m = malloc(XMSS_MLEN);
    //byte[] m_U = new byte[(int)dataAddress((MethodRef0<Integer>)ImplicitDeclarations::xmssMlen)];
    byte[] m_U = new byte[XMSS_MLEN];
    //byte sm = malloc(xmss_params_sig_bytes + XMSS_MLEN);
    byte[] sm_U = new byte[xmss_params_sig_bytes + XMSS_MLEN];
    //byte mout = malloc(xmss_params_sig_bytes + XMSS_MLEN);
    byte[] mout_U = new byte[xmss_params_sig_bytes + XMSS_MLEN];
    long smlen;
    long mlen;
    byte[] nodes = new byte [xmss_params_n * xmss_params_d * (1 << xmss_params_tree_height)];
    byte[] wots = new byte [(xmss_params_d - 1) * xmss_params_wots_sig_bytes];
 
 XMSSParameters xmssParams = new XMSSParameters(10, new SHA256Digest());
 try{	
 SecureRandom prng = SecureRandom.getInstance("SHA1PRNG", "SUN");

  SHA256Digest digest = new SHA256Digest();
        int n = digest.getDigestSize();
        byte[] secretKeySeed = new byte[n];
        prng.nextBytes(secretKeySeed);
        byte[] secretKeyPRF = new byte[n];
        prng.nextBytes(secretKeyPRF);
        byte[] publicSeed = new byte[n];
        prng.nextBytes(publicSeed);
        XMSS xmss = new XMSS(xmssParams, prng);
        xmss.generateKeys();
} catch (Exception e){

}

 // Obtain CardTerminal
       // 1. Create simulator and install applet
CardSimulator simulator = new CardSimulator();
AID appletAID = AIDUtil.create("F000000001");
simulator.installApplet(appletAID, XMSSNew.class);

// 2. Create Terminal
CardTerminal terminal = CardTerminalSimulator.terminal(simulator);

// 3. Connect to Card
try {
Card card = terminal.connect("T=1");
CardChannel channel = card.getBasicChannel();

// 4. Select applet
CommandAPDU selectCommand = new CommandAPDU(AIDUtil.select(appletAID));
channel.transmit(selectCommand);

// 5. Send APDU
CommandAPDU commandAPDU1 = new CommandAPDU(0x00, 0x01, 0x00, 0x00);

ResponseAPDU response = simulator.transmitCommand(commandAPDU1);
System.out.println("Response: sw: 0x" + Integer.toHexString(response.getSW()) + " data:" + response.getData()+"\n");
CommandAPDU commandAPDU2 = new CommandAPDU(0x00, 0x50, 0x00, 0x00);
response = simulator.transmitCommand(commandAPDU2);
System.out.println("Response: sw: 0x" + Integer.toHexString(response.getSW()) + " data:" + response.getData()+"\n");
CommandAPDU commandAPDU3 = new CommandAPDU(0x00, 0x51, 0x00, 0x00);
response = simulator.transmitCommand(commandAPDU3);
System.out.println("Response: sw: 0x" + Integer.toHexString(response.getSW()) + " data:" + response.getData()+"\n");
CommandAPDU commandAPDU4 = new CommandAPDU(0x00, 0x52, 0x00, 0x00);
response = simulator.transmitCommand(commandAPDU4);
System.out.println("Response: sw: 0x" + Integer.toHexString(response.getSW()) + " data:" + response.getData()+"\n");

CommandAPDU commandAPDU_upload_keypair_nodes_wots = new CommandAPDU(0x00, 0x80, 0x00, 0x00);



}catch (Exception e){
	e.printStackTrace();
}
// 6. Check response status word
//assertEquals(0x9000, response.getSW());

       }

}